# Survey Project Server 
This is demo for survey project which is built based on reactjs/mobx/ant design

- - -
## Tech Stack
* [NodeJS](https://nodejs.org/)
* [ReactJS](https://ja.reactjs.org/)
* [Mobx](https://mobx.js.org/README.html)
* [Ant Design](https://ant.design/index-cn)
* [Swagger](http://springfox.github.io/springfox/docs/current)
* [Docker](https://www.docker.com)

## Installation
#### Build porject
Please install nodejs/yarn on your laptop before start
```sh
git clone https://gitlab.com/my-survey-project/survey-ui.git
cd survey-ui
yarn build
```
#### Run application
```sh
yarn start
```
#### Build docker image and run by using docker-compose
```sh
docker-compose up -d
```
#### Access UI
[http://localhost:3000/](http://localhost:3000/)
- - -

