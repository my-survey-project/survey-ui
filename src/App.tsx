import './App.css';

import Layout from 'antd/lib/layout';
import * as React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import * as routes from './constants/routes';
// Routes
import Home from './containers/Home';
import Login from './containers/Login';
import Logout from './containers/Logout';
import CreateSurvey from './containers/CreateSurvey';
import Register from './containers/Register';
import AnswerSurvey from './containers/AnswerSurvey';
import CheckSurvey from './containers/CheckSurvey';

export default class App extends React.Component<{}, { collapsed: boolean }> {
    constructor(props: any) {
        super(props);
        this.state = {
            collapsed: false,
        };
    }

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    public render () {
        return (
            <div>
                <Layout>
                    <article className="container main">
                        <Switch>
                            <Route path={routes.LOGIN} component={Login} />
                            <Route path={routes.LOGOUT} component={Logout} />
                            <Route path={routes.REGISTER} component={Register} />
                            <Route path={routes.SURVEY_CREATE} component={CreateSurvey} />
                            <Route path={routes.SURVEY_ANSWER} component={AnswerSurvey} />
                            <Route path={routes.SURVEY_RESULT} component={CheckSurvey} />
                            <Route path={routes.HOME} component={Home} />
                            <Redirect to="/404" />
                        </Switch>
                    </article>
                </Layout>
            </div>
        );
    }
}
