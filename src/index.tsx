import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'antd/dist/antd.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { createBrowserHistory } from 'history';
import { Provider } from 'mobx-react';
import { syncHistoryWithStore } from 'mobx-react-router';
import { CookiesProvider } from 'react-cookie';
import { Router } from 'react-router-dom';

// Mobx Root Store
import allStore from './stores';

// Routing store
const browserHistory = createBrowserHistory();
const history = syncHistoryWithStore(browserHistory, allStore.routingStore);

ReactDOM.render(
  <Provider {...allStore}>
    <Router history={history}>
      <CookiesProvider>
        <App />
      </CookiesProvider>
    </Router>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
