import { Alert, Divider } from 'antd';
import Button from 'antd/lib/button';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Select from 'antd/lib/select'
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { Survey, SurveyAnswer } from '../../api';

import SinglePageForm from '../../components/SignlePageForm';

const { Option } = Select;

const AnswerSurvey = inject(
    'authStore',
    'surveyStore',
    'routingStore',
)(
    observer((props: any) => {
        const surveyId = props.match.params.surveyId;
        const [survey, setSurvey] = React.useState<Survey>();
        const [surveyAnswer, setSurveyAnswer] = React.useState<SurveyAnswer>()
        const [errorMessage, setErrorMessage] = React.useState<string | undefined>();

        React.useEffect(() => {
            props.surveyStore
                .getSurvey(surveyId)
                .then((surveyData: any) => {
                    const survey = surveyData.data;
                    setSurvey(survey);
                    setSurveyAnswer({
                        username: '',
                        email: '',
                        answers: survey.questions.map((_question: { index: number; }) => {
                            return {
                                questionIdx: _question.index,
                                optionIdx: -1,
                            }
                        })
                    })
                });
        }, [props.surveyStore, surveyId]);

        const handleErrorMessageClose = () => {
            setErrorMessage(undefined);
        };

        const handleModifyUsername = (event: any) => {
            event.preventDefault();
            const value = (event.target as any).value;
            setSurveyAnswer({
                ...surveyAnswer,
                username: value,
            });
        }

        const handleModifyEmail = (event: any) => {
            event.preventDefault();
            const value = (event.target as any).value;
            setSurveyAnswer({
                ...surveyAnswer,
                email: value,
            });
        }

        const handleAddAnswer = (value: any, questionIdx: number) => {
            setSurveyAnswer({
                ...surveyAnswer,
                answers: surveyAnswer?.answers?.map((answer, idx) => {
                    if (idx === questionIdx) {
                        return {
                            ...answer,
                            optionIdx: value
                        }
                    } else {
                        return answer;
                    }
                })
            });
        };

        const handleSubmitClick = async (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
            event.preventDefault();
            if (!surveyAnswer?.email) {
                setErrorMessage('Please input your email.');
                return
            }
            if (!surveyAnswer.username) {
                setErrorMessage('Please input you name');
                return
            }
            if (!surveyAnswer.answers) {
                setErrorMessage('Please answer all the questions');
                return
            }

            await props.surveyStore.answerSurvey(surveyId, surveyAnswer);
            props.routingStore.history.push(`/surveyResult/${surveyId}`);
        };

        const layout = {
            wrapperCol: { span: 24 },
        };

        return (
            <SinglePageForm pageHeader="Answer Survey">
                <Form {...layout}>
                    {errorMessage ? (
                        <Form.Item data-name="error">
                            <Alert
                                message={errorMessage}
                                type="error"
                                closable={true}
                                afterClose={handleErrorMessageClose}
                            />
                        </Form.Item>
                    ) : (
                            <div />
                        )}
                    <Form.Item>
                        <Input onChange={(e) => handleModifyUsername(e)} value={surveyAnswer?.username} placeholder="User Name" />
                    </Form.Item>
                    <Form.Item>
                        <Input onChange={(e) => handleModifyEmail(e)} value={surveyAnswer?.email} placeholder="Email" />
                    </Form.Item>
                    <Divider />
                    {
                        survey?.questions ?
                            survey?.questions.map((question, questionIdx) => {
                                return <div key={questionIdx}>
                                    <Form.Item>
                                        <Input disabled value={question.question} />
                                    </Form.Item>
                                    <Form.Item>
                                        <Select defaultValue="" style={{ width: 120 }} allowClear onSelect={(e) => { handleAddAnswer(e, questionIdx); }}>
                                            {question?.options?.map((option, optionIdx) => {
                                                return <Option value={optionIdx} key={optionIdx}>{option.content}</Option>;
                                            })}
                                        </Select>
                                    </Form.Item>
                                    <Divider />
                                </div>
                            }) : <div />
                    }
                    <Form.Item >
                        <Button type="primary" onClick={handleSubmitClick}>
                            {'Submit'}
                        </Button>
                    </Form.Item>
                </Form>
            </SinglePageForm >
        );
    })
);

export default AnswerSurvey;
