import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import * as routes from '../../constants/routes';

const Login = inject(
    'authStore',
    'routingStore'
)(
    observer((props: any) => {
        props.authStore.logout();
        return (
            <Redirect to={routes.HOME} />
        );
    })
);

export default Login;
