import Alert from 'antd/lib/alert';
import Button from 'antd/lib/button';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import validator from 'validator';

import SinglePageForm from '../../components/SignlePageForm';
import * as routes from '../../constants/routes';

const Signup = inject(
    'authStore',
    'routingStore'
)(
    observer((props: any) => {
        const [errorMessage, setErrorMessage] = React.useState<string | undefined>();

        const handleErrorMessageClose = () => {
            setErrorMessage(undefined);
        };

        const handleUsername = (event: any) => {
            const value = (event.target as any).value;
            props.authStore.user.username = value;
        };

        const handleEmail = (event: any) => {
            const value = (event.target as any).value;
            props.authStore.user.email = value;
        };

        const handlePassword = (event: any) => {
            const value = (event.target as any).value;
            props.authStore.user.password = value;
        };

        const handlePassword2 = (event: any) => {
            const value = (event.target as any).value;
            props.authStore.user.password2 = value;
        };

        const handleSubmitClick = async (
            event: React.MouseEvent<HTMLButtonElement, MouseEvent>
        ) => {
            event.preventDefault();
            if (!props.authStore.user.email || !validator.isEmail(props.authStore.user.email)) {
                setErrorMessage('Email is empty or formart is wrong');
                return
            }
            if (!props.authStore.user.username) {
                setErrorMessage('Username is empty');
                return
            }
            if (!props.authStore.user.password || props.authStore.user.password.lenght <= 6) {
                setErrorMessage('Password is too short (At least 6 digits)');
                return
            }
            if (props.authStore.user.password !== props.authStore.user.password2) {
                setErrorMessage('The two passwords not equal.');
                return
            }
            try {
                await props.authStore.register()
                props.routingStore.history.push(routes.HOME);
            } catch (e) {
                setErrorMessage(e.message);
            }

        };

        const layout = {
            wrapperCol: { span: 24 },
        };

        return (
            <SinglePageForm pageHeader="Register">
                <Form {...layout}>
                    {errorMessage ? (
                        <Form.Item data-name="error">
                            <Alert
                                message={errorMessage}
                                type="error"
                                closable={true}
                                afterClose={handleErrorMessageClose}
                            />
                        </Form.Item>
                    ) : (
                            <div />
                        )}
                    <Form.Item data-name="email">
                        <Input onChange={handleEmail} placeholder="Email" />
                    </Form.Item>
                    <Form.Item data-columntype="username">
                        <Input onChange={handleUsername} placeholder="User Name" />
                    </Form.Item>
                    <Form.Item data-columntype="password">
                        <Input.Password onChange={handlePassword} placeholder="Password" />
                    </Form.Item>
                    <Form.Item data-columntype="password2">
                        <Input.Password onChange={handlePassword2} placeholder="Confirm Password" />
                    </Form.Item>
                    <Form.Item className="is-text-right">
                        {'Have account?'} <a href="/login">{'Login'}</a>
                    </Form.Item>
                    <Form.Item className="is-text-right">
                        <Button type="primary" onClick={handleSubmitClick}>
                            {'Register'}
                        </Button>
                    </Form.Item>
                </Form>
            </SinglePageForm>
        );
    })
);

export default Signup;
