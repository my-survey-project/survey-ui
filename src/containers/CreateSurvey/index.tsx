import Alert from 'antd/lib/alert';
import { Col, Divider, Row } from 'antd';
import Button from 'antd/lib/button';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import TextArea from 'antd/lib/input/TextArea';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { Survey } from '../../api';

import SinglePageForm from '../../components/SignlePageForm';
import * as routes from '../../constants/routes';

const CreateSurvey = inject(
    'authStore',
    'surveyStore',
    'routingStore'
)(
    observer((props: any) => {
        if (typeof props.authStore.getMyInfo() === 'undefined') {
            return <Redirect to={routes.LOGIN} />;
        }

        const [survey, setSurvey] = React.useState<Survey>({
            id: 0,
            title: '',
            description: '',
            questions: [{
                index: 0,
                question: '',
                options: [{
                    index: 0,
                    content: ''
                }]
            }]
        });

        const [errorMessage, setErrorMessage] = React.useState<string | undefined>();

        const handleErrorMessageClose = () => {
            setErrorMessage(undefined);
        };
        
        const handleModifyTitle = (event: any) => {
            event.preventDefault();
            const value = (event.target as any).value;
            setSurvey({
                ...survey,
                title: value,
            });
        }

        const handleModifyDescription = (event: any) => {
            event.preventDefault();
            const value = (event.target as any).value;
            setSurvey({
                ...survey,
                description: value,
            });
        }

        const handleAddQuestion = (event: any) => {
            event.preventDefault();
            const questions = survey.questions ? survey.questions : [];
            setSurvey({
                ...survey, questions: [...questions, {
                    index: questions.length | 0,
                    question: '',
                    options: []
                }]
            });
        };

        const handleAddOptions = (event: any, questionIdx: any) => {
            event.preventDefault();
            const questions = survey.questions ? survey.questions : [{
                index: 0,
                question: '',
                options: [{
                    index: 0,
                    content: ''
                }]
            }];
            setSurvey({
                ...survey, questions: questions.map((question, idx) => {
                    const options = question.options ? question.options : [{
                        index: 0,
                        content: ''
                    }];
                    if (idx === questionIdx) {
                        return {
                            ...question, options: [...options, {
                                index: options.length,
                                content: ''
                            }]
                        };
                    } else {
                        return question;
                    }
                })
            });
        }

        const handleModifyQuestion = (event: any, questionIdx: any) => {
            event.preventDefault();
            const questions = survey.questions ? survey.questions : [];
            const value = (event.target as any).value;
            setSurvey({
                ...survey, questions: questions.map((question, idx) => {
                    if (idx === questionIdx) {
                        return { ...question, question: value };
                    } else {
                        return question;
                    }
                })
            });
        }

        const handleModifyOption = (event: any, questionIdx: any, optionIdx: any) => {
            event.preventDefault();
            const questions = survey.questions ? survey.questions : [];
            const value = (event.target as any).value;
            setSurvey({
                ...survey, questions: questions.map((question, idx1) => {
                    const options = question.options ? question.options : [];
                    if (idx1 === questionIdx) {
                        return {
                            ...question, options: options.map((option, idx2) => {
                                if (idx2 === optionIdx) {
                                    return { ...option, content: value }
                                } else {
                                    return option
                                }
                            })
                        };
                    } else {
                        return question;
                    }
                })
            });
        }

        const handleSubmitClick = async (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
            event.preventDefault();
            if (!survey.title) {
                setErrorMessage('Title can not be empty.');
                return
            }
            if (!survey.description) {
                setErrorMessage('Description can not be empty');
                return
            }
            if (!survey.questions) {
                setErrorMessage('Must create at least one question');
                return  
            }

            for (var i = 0; i < survey.questions.length; i++) {
                if (!survey.questions[i].question) {
                    setErrorMessage('Question can not be empty');
                    return
                }
                if (!survey.questions[i].options) {
                    setErrorMessage('Must create at least one option per question');
                    return  
                }
            }

            survey.questions?.forEach(question => {
                if (!question.question) {
                    setErrorMessage('Question can not be empty');
                    return
                }
                question.options?.forEach(option => {
                    if (!option.content) {
                        setErrorMessage('Question can not be empty');
                        return
                    }
                })
            });
            await props.surveyStore.createSurvey(survey);
            props.routingStore.history.push(routes.HOME);
        };

        const layout = {
            wrapperCol: { span: 24 },
        };

        return (
            <SinglePageForm pageHeader="Create Survey">
                <Form {...layout}>
                {errorMessage ? (
                        <Form.Item data-name="error">
                            <Alert
                                message={errorMessage}
                                type="error"
                                closable={true}
                                afterClose={handleErrorMessageClose}
                            />
                        </Form.Item>
                    ) : (
                            <div />
                        )}
                    <Form.Item {...layout}>
                        <Input onChange={(e) => handleModifyTitle(e)} value={survey.title} placeholder="Survey Title" />
                    </Form.Item>
                    <Form.Item {...layout}>
                        <TextArea
                            onChange={(e) => handleModifyDescription(e)}
                            value={survey.description}
                            placeholder="Survey Description"
                            autoSize={{ minRows: 3, maxRows: 5 }}
                        />
                    </Form.Item>
                    <Divider />
                    {
                        survey?.questions ?
                            survey?.questions.map((question, questionIdx) => {
                                return <div key={questionIdx}>
                                    <Form.Item>
                                        <Row justify="space-between">
                                            <Col span={16}><Input onChange={(e) => handleModifyQuestion(e, questionIdx)} value={question.question} placeholder={`Question ${questionIdx + 1}`} /></Col>
                                            <Col>
                                                <Button type="primary" onClick={(e) => handleAddOptions(e, questionIdx)}>
                                                    {'Add Option'}
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Form.Item>
                                    {question?.options?.map((option, optionIdx) => {
                                        return <Form.Item key={`${questionIdx}_${optionIdx}`}>
                                            <Input onChange={(e) => handleModifyOption(e, questionIdx, optionIdx)} value={option.content} placeholder={`Option ${optionIdx + 1}`} />
                                        </Form.Item>
                                    })}
                                    <Divider />
                                </div>
                            }) : <div />
                    }
                    <Form.Item className="is-text-right">
                        <Button type="primary" onClick={handleAddQuestion}>
                            {'Add Question'}
                        </Button>
                    </Form.Item>
                    <Form.Item className="is-text-right">
                        <Button type="primary" onClick={handleSubmitClick}>
                            {'Submit'}
                        </Button>
                    </Form.Item>
                </Form>
            </SinglePageForm>
        );
    })
);

export default CreateSurvey;
