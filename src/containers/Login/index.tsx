import Alert from 'antd/lib/alert';
import Button from 'antd/lib/button';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Row from 'antd/lib/row';
import { inject, observer } from 'mobx-react';
import * as React from 'react';

import SinglePageForm from '../../components/SignlePageForm';
import * as routes from '../../constants/routes';

const Login = inject(
    'authStore',
    'routingStore'
)(
    observer((props: any) => {
        const [errorMessage, setErrorMessage] = React.useState<string | undefined>();
        const handleErrorMessageClose = () => {
            setErrorMessage(undefined);
        };

        const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
            event.preventDefault();
            props.authStore.user.email = event.target.value;
        };

        const handlePwdChange = (event: React.ChangeEvent<HTMLInputElement>) => {
            event.preventDefault();
            props.authStore.user.password = event.target.value;
        };

        const handleSubmitClick = async (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
            event.preventDefault();
            try {
                await props.authStore.login();
                props.routingStore.history.push(routes.HOME);
            } catch (e) {
                setErrorMessage(e.message);
            }
        };

        const layout = {
            wrapperCol: { span: 24 },
        };

        const LoginForm = () => (
            <Form {...layout}>
                {errorMessage ? (
                    <Row>
                        <Form.Item data-name="error">
                            <Alert
                                message={errorMessage}
                                type="error"
                                closable={true}
                                afterClose={handleErrorMessageClose}
                            />
                        </Form.Item>
                    </Row>
                ) : (
                        <div />
                    )}
                <Form.Item>
                    <Input placeholder="Email" onChange={handleEmailChange} />
                </Form.Item>
                <Form.Item>
                    <Input.Password placeholder="Password" onChange={handlePwdChange} />
                </Form.Item>
                <Form.Item className="is-text-right">
                    {'Haven\'t registered?'} <a href={routes.REGISTER}>{'Register'}</a>
                </Form.Item>
                <Form.Item className="is-text-right">
                    <Button type="primary" onClick={handleSubmitClick}>
                        {'Login'}
                    </Button>
                </Form.Item>
            </Form>
        );

        return <SinglePageForm pageHeader="Login">{LoginForm()}</SinglePageForm>;
    })
);

export default Login;
