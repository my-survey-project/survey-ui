/* eslint-disable react-hooks/exhaustive-deps */
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Table from 'antd/lib/table/Table';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { Survey, SurveyAnswer } from '../../api';

import SinglePageForm from '../../components/SignlePageForm';

const { TextArea } = Input;

const CheckSurvey = inject(
    'authStore',
    'surveyStore'
)(
    observer((props: any) => {
        const surveyId = props.match.params.surveyId;
        const [survey, setSurvey] = React.useState<Survey>();
        const [columns, setColumns] = React.useState<any>()
        const [data, setData] = React.useState<any>()

        React.useEffect(() => {
            const convertSurveyAnswerToData = (survey: any, surveyAnswers: any[]) => {
                return surveyAnswers.map(surveyAnswer => {
                    const others = (surveyAnswer.answers?.map((answer: any) => {
                        return {
                            questionIdx: answer.questionIdx,
                            content: answer.optionIdx !== -1 ?
                                survey.questions[answer.questionIdx].options[answer.optionIdx].content : ''
                        };
                    }));
                    var result = others.reduce(function (map: any, obj: any) {
                        map[obj.questionIdx] = obj.content;
                        return map;
                    }, {});

                    return {
                        key: `${surveyAnswer.username} | ${surveyAnswer.email}`,
                        name: surveyAnswer.username,
                        email: surveyAnswer.email,
                        ...result
                    }

                })
            }

            const convertSurveyToColumns = (survey: Survey) => {
                let questionColumns = survey?.questions?.map(question => {
                    return {
                        title: question.question,
                        dataIndex: question.index,
                        key: question.index,
                    }
                });
                questionColumns = questionColumns ? questionColumns : [];

                return [
                    {
                        title: 'User Name',
                        dataIndex: 'name',
                        key: 'name',
                    },
                    {
                        title: 'Email',
                        dataIndex: 'email',
                        key: 'email',
                    },
                    ...questionColumns
                ];
            }

            props.surveyStore
                .getSurvey(surveyId)
                .then((surveyData: any) => {
                    const survey: Survey = surveyData.data;
                    setSurvey(survey);
                    setColumns(convertSurveyToColumns(survey));

                    props.surveyStore
                        .getSurveyAnswers(surveyId)
                        .then((surveyAnswersData: any) => {
                            const surveyAnswers: SurveyAnswer[] = surveyAnswersData.data;
                            console.log(convertSurveyAnswerToData(survey, surveyAnswers));
                            setData(convertSurveyAnswerToData(survey, surveyAnswers))
                        });
                });
        }, []);

        const layout = {
            wrapperCol: { span: 24 },
        };

        return (
            <SinglePageForm pageHeader="Check Survey Result">
                <Form {...layout}>
                    <Form.Item>
                        <Input disabled value={survey?.title} />
                    </Form.Item>
                    <Form.Item>
                        <TextArea disabled value={survey?.description} autoSize={{ minRows: 3, maxRows: 5 }} />
                    </Form.Item>
                    <Form.Item>
                        <Table dataSource={data} columns={columns} />
                    </Form.Item>
                </Form>
            </SinglePageForm>
        );
    })
);

export default CheckSurvey;
