import { inject, observer } from 'mobx-react';
import * as React from 'react';
import SurveyTable from '../../components/SurveyTable';

import { PageSurvey } from '../../api/';
import SinglePageForm from 'components/SignlePageForm';
import { Redirect } from 'react-router-dom';

import * as routes from '../../constants/routes';

const SurveyContainer = inject('surveyStore', 'authStore')(
    observer((props: any) => {
        const [pageSurveys, setPageSurveys] = React.useState<PageSurvey>();
        const [logined, setLogined] = React.useState<boolean>(true);

        React.useEffect(() => {
            props.surveyStore
                .getSurveys(0, 20)
                .then((pageChangeData: any) => {
                    if (pageChangeData) {
                        setPageSurveys(pageChangeData.data);
                    } else {
                        setLogined(false);
                    }
                });
        }, [props.surveyStore]);

        return (
            logined ? <SinglePageForm pageHeader="All Surveys">
                {pageSurveys ? <SurveyTable pageSurvey={pageSurveys}></SurveyTable> : <div />}
            </SinglePageForm> : <Redirect to={routes.LOGIN} />

        );
    })
);

export default SurveyContainer;
