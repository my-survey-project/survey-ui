/* eslint-disable jsx-a11y/anchor-is-valid */
import Table from 'antd/lib/table';
import * as React from 'react';
import { PageSurvey } from '../../api/';

interface ISurveyTableProps {
    pageSurvey: PageSurvey;
}

const SurveyTable: React.FunctionComponent<ISurveyTableProps> = (props) => {
    const data = props.pageSurvey.content?.map(survey => {
        return {
            key: survey.id?.toString(),
            title: survey.title,
            description: survey.description,
            name: ''
        }
    });
    const columns = [
        {
            title: 'Survey title',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Survey description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Detail',
            key: 'detail',
            render: (_text: any, record: any) => (
                <a href={`/surveyResult/${record.key}`}>Check {record.name}</a>
            ),
        },
    ];
    return (
        <div>
            <Table columns={columns} dataSource={data} />
        </div>
    );
};

export default SurveyTable;
