import Col from 'antd/lib/col';
import Divider from 'antd/lib/divider';
import { Layout, Menu } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserAddOutlined,
  LoginOutlined,
  UnorderedListOutlined,
  AppstoreOutlined,
  LogoutOutlined,
  FormOutlined,
} from '@ant-design/icons';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { isMobile } from 'react-device-detect';
import SubMenu from 'antd/lib/menu/SubMenu';
import * as routes from '../../constants/routes';

const { Header, Sider, Content } = Layout;

const SinglePageForm = inject("authStore", "routingStore")(
  observer((props: any) => {
    const [collapsed, setCollapsed] = React.useState<boolean>(isMobile);
    const toggle = () => {
      setCollapsed(!collapsed);
    };

    const handleClick = async (e: any) => {
      console.log(props.routingStore);
      switch (e.key) {
        case 'allSurveys':
          props.routingStore.history.push(routes.SURVEYS);
          break
        case 'createSurvey':
          props.routingStore.history.push(routes.SURVEY_CREATE);
          break
        case 'login':
          props.routingStore.history.push(routes.LOGIN);
          break
        case 'logout':
          await props.authStore.logout();
          props.routingStore.history.push(routes.LOGIN);
          break
        case 'register':
          props.routingStore.history.push(routes.REGISTER);
          break
      }
    };

    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} onClick={handleClick}>
            {
              props.authStore.getMyInfo() ?
                [<SubMenu key="surveys" icon={<AppstoreOutlined />} title="Surveys">
                  <Menu.Item key="allSurveys" icon={<UnorderedListOutlined />}>
                    {'All Surveys'}
                  </Menu.Item>
                  <Menu.Item key="createSurvey" icon={<FormOutlined />}>
                    {'New Survey'}
                  </Menu.Item>
                </SubMenu>, <Menu.Item key="logout" icon={<LogoutOutlined />}>
                  {'Logout'}
                </Menu.Item>]
                :
                [<Menu.Item key="login" icon={<LoginOutlined />}>
                  {'Login'}
                </Menu.Item>, <Menu.Item key="register" icon={<UserAddOutlined />}>
                  {'Register'}
                </Menu.Item>]
            }

          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: toggle,
            })}
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            <h3>{props.pageHeader}</h3>
            <Divider />
            <Col sm={{ span: 18, offset: 2 }} md={{ span: 16, offset: 4 }}>
              {props.children}
            </Col>
          </Content>
        </Layout>
      </Layout>
    );
  })
);

export default SinglePageForm;
