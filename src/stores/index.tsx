import { RouterStore } from 'mobx-react-router';

// Stores
import AuthStore from './AuthStore';
import SurveyStore from './SurveyStore';

// Mobx Root Store
let routingStore = new RouterStore();
let authStore = new AuthStore();
let surveyStore  = new SurveyStore();

const allStore = () => ({
    routingStore,
    authStore,
    surveyStore
});

export default allStore();
