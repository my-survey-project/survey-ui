import { action, observable } from 'mobx';

import { Survey, SurveyAnswer, SurveyApi } from '../api';

const surveyApi = new SurveyApi();
export default class AuthStore {
    @observable inProgress = false;
    @observable errorMsg = '';


    @action async createSurvey(survey: Survey) {
        try {
            return await surveyApi.createSurvey(survey)
        } catch (e) {
            this.errorMsg = e.message;
        }
    }

    @action async getSurveys(offset: number, limit: number) {
        try {
            return await surveyApi.getSurveys(limit, offset)
        } catch (e) {
            this.errorMsg = e.message;
        }
    }

    @action async getSurvey(surveyId: string) {
        try {
            return await surveyApi.getSurvey(surveyId)
        } catch (e) {
            this.errorMsg = e.message;
        }
    }

    @action async answerSurvey(surveyId: string, surveyAnswer: SurveyAnswer) {
        try {
            return await surveyApi.answerSurvey(surveyId, surveyAnswer)
        } catch (e) {
            this.errorMsg = e.message;
        }
    }

    @action async getSurveyAnswers(surveyId: string) {
        try {
            return await surveyApi.getSurveyResult(surveyId)
        } catch (e) {
            this.errorMsg = e.message;
        }
    }
}
