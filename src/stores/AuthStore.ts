import { action, observable } from 'mobx';

import { User, UsersAndAuthenticationApi } from '../api';

const userAndAuthenticationApi = new UsersAndAuthenticationApi();
export default class AuthStore {
    @observable inProgress = false;
    @observable errorMsg = '';

    @observable user = {
        username: '',
        email: '',
        password: '',
        password2: ''
    };

    @observable my: User | undefined = undefined;

    @action async login () {
        console.log(this.user);
        await userAndAuthenticationApi.login(
            this.user.email,
            this.user.password
        );
        await this.syncMyInfo();
    }

    @action async logout () {
        await userAndAuthenticationApi.logout();
        localStorage.removeItem('my');
    }

    @action getMyInfo () {
        const my = localStorage.getItem('my');
        return my ? JSON.parse(my) : undefined;
    }

    @action async syncMyInfo () {
        const user = await userAndAuthenticationApi.getUserInfo('my');
        if (user) {
            localStorage.setItem('my', JSON.stringify(user.data));
        }
    }

    @action async register () {
        const self = this;

        await userAndAuthenticationApi.createUser(this.user).then(
            async () => {
                await self.login();
            }
        )
    }
}
